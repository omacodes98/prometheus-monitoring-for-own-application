# Prometheus - Configure Monitoring for Own Application

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

* Configure NodeJS application to collect & expose Metrics with Prometheus Client Library 

* Deploy the NodeJS application, which has a metrics endpoint configured, into Kubernetes cluster 

* Configure Prometheus to scrape this exposed metrics and visualize in Grafana Dahboard 

## Technologies Used 

* Prometheus 

* Kubernetes 

* Node.js 

* Grafana 

* Docker 

* Docker Hub 

## Steps 

Step 01: Use npm to install all dependencies and run application

    npm install 

[installing dependecies](/images/01%20use%20npm%20to%20install%20all%20dependencies%20and%20run%20application.png)

[application on browser](/images/02%20application%20on%20browser.png)

[docker image that will be deployed in k8 cluster](/images/03%20application%20that%20will%20be%20deployed%20on%20kubernetes.png)

Step 02: Create a file for kubernetes with configuration for deployment and service of application

[k8 configuration file](/images/04%20create%20a%20file%20for%20kubernetes%20with%20configuration%20for%20deployment%20and%20service%20of%20application.png)

Step 03: Create secret for your password with flags like username password and docker server url

    kubectl create secret docker-registry my-registry-key --docker-server=https://index.docker.io/v1/ --docker-username=omacodes98 --docker-password=xxxxxxxxxx

[Creating secret](/images/05%20create%20secret%20for%20your%20password%20with%20flags%20like%20username%20password%20and%20docker%20server%20url.png)

Step 04: Apply configuration file 

    kubectl apply -f k8-config.yaml 

[applying configuration file](/images/06%20apply%20configuration%20file.png)

[pod running](/images/07%20pod%20running.png)

Step 05: To make sure everything is working fine port forward app service to bind on localhost

    kubectl port-forward svc/nodeapp 3000:3000

[port forward app svc](/images/08%20to%20make%20sure%20everything%20is%20working%20fine%20port%20forward%20app%20service%20to%20bind%20on%20localhost.png)

[App on browser](/images/09%20success%20on%20browser.png)

Step 06: Go back to configuration file to create a new component which is the servicemonitor component that will inform prometheus about the new endpoint to scrap

[ServiceMonitor](/images/10%20Go%20back%20to%20configuration%20file%20to%20create%20a%20new%20component%20which%20is%20the%20servicemonitor%20component%20that%20will%20inform%20prometheus%20about%20the%20new%20endpoint%20to%20scrap.png)

Step 07: Set apiversion to monitoring.coreos.com/v1 and kind to ServiceMonitor because that is the cluster component we are creating

[Apiversion](/images/11%20set%20apiversion%20to%20monitoring%20coreos%20dot%20com%20slash%20v1%20and%20kind%20to%20ServiceMonitor%20because%20that%20is%20the%20cluster%20component%20we%20are%20creating.png)

Step 08: Configure metadate, give this component a name for the cluster and label to make sure prometheus is able to match all the service monitors in the cluster

[metadata](/images/12%20configure%20metadate_%20give%20this%20component%20a%20name%20for%20the%20cluster%20and%20label%20to%20make%20sure%20prometheus%20is%20able%20to%20match%20all%20the%20service%20monitors%20in%20the%20cluster.png)

Step 08: Configure spec with endpoint attributein which you will give path to metrics and the name of port for the app and lastly targetPort

[spec](/images/13%20configure%20spec%20with%20spec%20attribute%20in%20which%20in%20which%20you%20will%20give%20oath%20to%20metrics%20and%20the%20name%20of%20port%20for%20the%20app%20and%20lastly%20targetPort%20.png)

Step 09: Add namespaceselector as part of the configuration for spec and set it to default, this is because prometheus is in a different name space from app

[namespace](/images/14%20Add%20namespaceselector%20as%20part%20of%20the%20cnfiguration%20for%20spec%20and%20set%20it%20to%20default_%20this%20is%20because%20prometheus%20is%20in%20a%20different%20name%20space%20from%20app.png)

Step 10: Add selector matchlabel in spec to enable k8 to find app that has the specific label you give it

[matchlabel](/images/15%20Add%20selector%20matchlabel%20in%20spec%20to%20enable%20k8%20to%20find%20app%20that%20has%20the%20specific%20label%20you%20give%20it%20.png)

Step 11: Apply changes

    kubectl apply -f k8-config.yaml 

[Applying changes](/images/16%20apply%20changes.png)

[prometheus ui target](/images/17%20prometheus%20ui%20target%20success%20prometheus%20is%20now%20monitoring%20the%20app.png)

Step 12: Go to grafana and create a dashboard, in metrics browser insert metrics that monitors http requests

[Grafana configuration](/images/18%20go%20to%20grafana%20and%20create%20a%20dashoard_in%20metrics%20rowser%20insert%20metrics%20that%20monitors%20http%20requests.png)

Step 13: Save dashboard and check graph success

[grah showcase](/images/19%20save%20dashboard%20and%20check%20graph%20success.png)

## Installation

    brew install eksctl 

## Usage 

    kubectl apply -f k8s-config.yaml

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/prometheus-monitoring-for-own-application.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/prometheus-monitoring-for-own-application

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.